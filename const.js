const mysql = require('mysql');

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'vsk',
  password: 'vsk',
  database: 'shoppingCart',
  socketPath: '/Applications/MAMP/tmp/mysql/mysql.sock',
  port: 8889
});
connection.connect((err) => {
  if (!err) {
      console.log('Database is connected ... nn');    
  } else {
      console.log('Error connecting database ... nn');    
  }
});
module.exports.connection = connection;

