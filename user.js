const connection = require('./const').connection;

let FinalResult = '';

const SelectAllUser = (req, res) => {
 connection.query('SELECT user_id, full_name, email_id, user_name, agree_check from tblUser', (error, rows) => {  
         if (!error) {
                FinalResult = { status: 'success', record_count: rows.length, records: rows };
                res.json(FinalResult);
            } else {
                FinalResult = { status: 'Failure', Msg: 'Error in fetching User' };
                res.json(FinalResult);
            }
    });
};

const insertUser = (insertJSON, res) => {
    const query = `INSERT INTO tblUser( full_name, email_id, user_name, user_password, agree_check) values('${insertJSON.name}','${insertJSON.emailId}','${insertJSON.userName}','${insertJSON.password}','${insertJSON.agreeCheck}')`;
    connection.query(query, (error) => {
        if (error) {
            FinalResult = { status: 'failure', messege: 'Insertion Failed' };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'success', messege: 'Inserted Successfully' };
            res.json(FinalResult);
        }
    });
};


const SelectUser = (req, res) => {
    const UserID = req.params.id;
    connection.query('SELECT * FROM tblUser WHERE user_id = ?', [UserID], (error, rows) => {
        console.log(connection.query);
        if (!error) {
            FinalResult = { status: 'success', record_count: rows.length, records: rows };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'Failure', Msg: 'Error in fetching User' };
            res.json(FinalResult);
        }
    });
};    

const updateUser = (updateJSON, res) => {
    const query = `UPDATE tblUser SET firstName='${updateJSON.firstName}', lastName='${updateJSON.lastName}', phoneNumber='${updateJSON.phoneNumber}', userType='${updateJSON.userType}', user_password='${updateJSON.password}' WHERE user_id='${updateJSON.userID}'`;
    connection.query(query, (error) => {
        if (error) {
            FinalResult = { status: 'failure', messege: 'Insertion Failed' };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'success', messege: 'Updated Successfully' };
            res.json(FinalResult);
        }
    });
};


const AdminLogin = (PostJson, res) => {
    const query = `SELECT adminID,userName,userPassword	 FROM tblAdmin WHERE userName='${PostJson.userName}' AND userPassword='${PostJson.password}'`;
    connection.query(query, (error, rows) => {
        if (!error) {
            if (rows.length !== 0) {
                FinalResult = { status: 'success', record_count: rows.length, records: rows };
                res.json(FinalResult);
            } else {
                FinalResult = { status: 'Failure', Msg: 'Username and Password incorrect' };
                res.json(FinalResult);
            }
        } else {
            FinalResult = { status: 'Failure', Msg: 'Error in fetching product' };
            res.json(FinalResult);
        }
    });
};   

const generateotp = (PostJson, res) => {
    const passcode=randNumGen(4);
    // const query = `INSERT INTO tblTempPhonePasscode( phoneNumber, passCode) values('${PostJson.PhoneNumber}','${passcode}')`;
    // connection.query(query, (error, rows) => {
        if (passcode !="") {
            FinalResult = { status: 'Success', passcode: passcode };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'Failure', Msg: 'Error in Generating OTP' };
            res.json(FinalResult);
        }
    // });
};   

const createuserapp = (insertJSON, res) => {
    const query = `INSERT INTO tblUser( phoneNumber, user_password ) values('${insertJSON.PhoneNumber}','${insertJSON.Password}')`;
    connection.query(query, (error, result) => {
        if (error) {
            FinalResult = { status: 'failure', messege: 'Insertion Failed' };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'Success', messege: 'Inserted Successfully', UserID: result.insertId };
            res.json(FinalResult);
        }
    });
};

const updateuserapp = (updateJSON, res) => {
    const query = `UPDATE tblUser SET firstName='${updateJSON.FirstName}',lastName='${updateJSON.LastName}',userType='${updateJSON.UserType}' WHERE user_id='${updateJSON.UserID}'`;
    connection.query(query, (error) => {
        if (error) {
            FinalResult = { status: 'failure', messege: 'Failed to Update' };
            res.json(FinalResult);
        } else {
            connection.query('SELECT user_id AS UserID,firstName as FirstName, lastName AS LastName, phoneNumber, userType AS Gender FROM tblUser WHERE user_id = ?', [updateJSON.UserID], (err, rows) => {
                console.log(connection.query);
                if (!err) {
                    FinalResult = { status: 'Success', record_count: rows.length, records: rows };
                    res.json(FinalResult);
                } else {
                    FinalResult = { status: 'Failure', Msg: 'Error in fetching User' };
                    res.json(FinalResult);
                }
            });
        }
    });
};

const updatepassword = (updateJSON, res) => {
    const query = `UPDATE tblUser SET user_password='${updateJSON.Password}' WHERE phoneNumber='${updateJSON.PhoneNumber}'`;
    connection.query(query, (error, result) => {
        if (error) {
            FinalResult = { status: 'failure', messege: 'Failure' };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'Success', messege: 'Updated Successfully' };
            res.json(FinalResult);
        }
    });
};

const checkuserexist = (updateJSON, res) => {
    const selectquery = `SELECT user_id FROM tblUser WHERE phoneNumber ='${updateJSON.PhoneNumber}'`;
    connection.query(selectquery, (error, rows) => {
        if (!error) {
            FinalResult = { status: 'Success', record_count: rows.length, records: rows };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'Failure', Msg: 'Error in fetching User' };
            res.json(FinalResult);
        }
    });
};

const loginUserApp = (PostJson, res) => {
    const query = `SELECT user_id AS UserID,firstName as FirstName, lastName AS LastName, phoneNumber, userType AS Gender FROM tbluser WHERE phoneNumber='${PostJson.PhoneNumber}' AND user_password='${PostJson.Password}'`;
    connection.query(query, (error, rows) => {
        if (!error) {
            FinalResult = { status: 'Success', record_count: rows.length, records: rows };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'Failure', Msg: 'Error in fetching product' };
            res.json(FinalResult);
        }
    });
}; 

function randNumGen() {
  var text = "";
  var possible = "123456789";
  for (var i = 0; i < 4; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

module.exports.insertUser = insertUser;
module.exports.SelectUser = SelectUser;
module.exports.updateUser = updateUser;
module.exports.SelectAllUser = SelectAllUser;
module.exports.AdminLogin = AdminLogin;
module.exports.generateotp = generateotp;
module.exports.createuserapp = createuserapp;
module.exports.updateuserapp = updateuserapp;
module.exports.updatepassword = updatepassword;
module.exports.checkuserexist = checkuserexist;
module.exports.loginUserApp = loginUserApp;