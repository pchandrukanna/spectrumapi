const connection = require('./const').connection;
 
const SelectAllOrders = (req, res) => {  
    let FinalResult = '';
    const checkoutArr = [];
    let orderArr = [];
    let productArr = [];
    const TotalOrderAmountArr = [];
    let checkoutIDs = '';
    let productIds = '';
    connection.query('SELECT CheckOutID, Name, Email, Telephone, Address, Country, State, City, postalCode, PayMode, userID FROM tblCheckout ORDER BY CheckOutID DESC', (error, rows) => { 
        if (!error) {
            for (let i = 0; i < rows.length; i++) {
                checkoutArr.push(rows[i]);
                checkoutIDs += `${rows[i].CheckOutID},`;
            }
            checkoutIDs = checkoutIDs.slice(0, -1);
            connection.query(`SELECT orderID, productID, checkOutID, itemQuantity,orderDate FROM tblOrder WHERE checkOutID in (${checkoutIDs})`, (err, orderRows) => {
                if (!err) {
                    for (let i = 0; i < checkoutArr.length; i++) {
                        orderArr = [];
                        for (let j = 0; j < orderRows.length; j++) {
                            productIds += `${orderRows[j].productID},`;
                            if (checkoutArr[i].CheckOutID === orderRows[j].checkOutID) {
                                orderArr.push(orderRows[j]);
                            }
                        }
                        checkoutArr[i].orders = orderArr;
                    }
                    productIds = productIds.slice(0, -1);
                    connection.query(`SELECT tblC.category_name, tblSC.subCategory_name, tblP.product_id, tblP.category_id, tblP.sub_category_id, tblP.brand_id, tblP.product_name, tblP.product_desc, tblP.productImg, tblP.productImg1, tblP.productImg2, tblP.productImg3, tblP.productImg4, tblP.productImg5, tblP.isActive, tblP.productCost, tblP.productCurrency, tblP.productQty, tblP.productSize, tblP.productSex, tblP.percentDiscount, tblP.finalAmount FROM tblProduct tblP INNER JOIN tblCategory tblC ON tblC.category_id = tblP.category_id INNER JOIN tblSubCategory tblSC ON tblSC.subCategoryID = tblP.sub_category_id WHERE tblP.product_id in (${productIds})`, (errProduct, product) => {
                        if (!errProduct) {
                            for (let i = 0; i < checkoutArr.length; i++) {
                                let sum = 0;
                                const orders = checkoutArr[i].orders;
                                for (let j = 0; j < orders.length; j++) {
                                    productArr = [];
                                    
                                    for (let k = 0; k < product.length; k++) {
                                     if (orders[j].productID === product[k].product_id) {                                 
                                         productArr.push(product[k]);
                                          const TotalAmount = orders[j].itemQuantity * product[k].finalAmount;
                                          orders[j].TotalAmount = TotalAmount;
                                          sum += TotalAmount;
                                        //   TotalOrderAmountArr.push(orders[j].TotalAmount);
                                        //   const test = TotalOrderAmountArr.reduce((a, b) => a + b, 0);
                                           checkoutArr[i].TotalOrderAmount = sum;
                                        }
                                    }
                                    orders[j].products = productArr;
                                }
                                checkoutArr[i].orders = orders;
                            }
                            
  
                            FinalResult = { status: 'success', record_count: rows.length, records: checkoutArr };
                            res.json(FinalResult);
                        } else {
                            FinalResult = { status: 'Failure', Msg: 'Error in fetching product' };
                            res.json(FinalResult);
                        }
                    });
                } else {
                    FinalResult = { status: 'Failure', Msg: 'Error in fetching product' };
                    res.json(FinalResult);
                }
            });
        } else {
            FinalResult = { status: 'Failure', Msg: 'Error in fetching product' };
            res.json(FinalResult);
        }
    });
};

const insertOrder = (insertJSON, res) => {
    // res.send(InsertJSON);
    let FinalResult = '';
    connection.beginTransaction((err) => {
        if (err) {
            throw err;
        }
        const query = `INSERT INTO tblCheckout(Name, Telephone, Address, PayMode, userID) VALUES ('${insertJSON.UserName}','${insertJSON.PhoneNumber}','${insertJSON.Address}','cash','${insertJSON.UserID}')`;
        connection.query(query, (error, result) => {
            if (error) {
                return connection.rollback(() => {
                    throw error;
                });
            }
            // const sizeObj = eval(`(${insertJSON.SizeArray})`);
            const sizequery = `INSERT INTO tblOrder(productID, checkOutID, itemQuantity, orderDate) VALUES ('${insertJSON.ProductID}','${result.insertId}','${insertJSON.Quantity}',NOW())`;
            connection.query(sizequery, (error1) => {
                if (error1) {
                    return connection.rollback(() => {
                        throw error1;
                    });
                }
                // this is ProductAddInfo Update
                const SelectProductAddInfo = `SELECT qty FROM tblProductAddInfo WHERE productID='${insertJSON.ProductID}' AND addInfoValue='${insertJSON.size}'`;
                connection.query(SelectProductAddInfo, (error2, rows2) => {
                    if (error2) {
                        return connection.rollback(() => {
                            throw error2;
                        });
                    }
                    if (rows2.length > 0) {
                        const itemQuty = rows2[0].qty - insertJSON.Quantity;
                        const UpdateQuery = `UPDATE tblProductAddInfo SET qty='${itemQuty}' WHERE productID = '${insertJSON.ProductID}' AND addInfoValue = '${insertJSON.size}'`;
                        connection.query(UpdateQuery, (error3) => {
                            if (error3) {
                                return connection.rollback(() => {
                                    throw error3;
                                });
                            }
                            //this is Product Update
                            const SelectProduct = `SELECT productQty FROM tblProduct WHERE product_id='${insertJSON.ProductID}'`;
                            connection.query(SelectProduct, (error4, Productrows) => {
                                if (error4) {
                                    return connection.rollback(() => {
                                        throw error4;
                                    });
                                }
                                if (Productrows.length > 0) {
                                    const ProductQuty = Productrows[0].productQty - insertJSON.Quantity;
                                    const UpdateProductQuery = `UPDATE tblProduct SET productQty='${ProductQuty}' WHERE product_id='${insertJSON.ProductID}'`;
                                    connection.query(UpdateProductQuery, (error5) => {
                                        if (error5) {
                                            return connection.rollback(() => {
                                                throw error5;
                                            });
                                        }
                                    });
                                }
                            });
                            // End Product Update                             
                        });
                    }
                });
                // End ProductAddInfo Update
                connection.commit((queryerror) => {
                    if (queryerror) {
                        return connection.rollback(() => {
                            throw queryerror;
                        });
                    }
                });
            });
            FinalResult = {
                status: 'Success',
                messege: 'Inserted Successfully'
            };
            res.json(FinalResult);
        });
    });
};

const CheckoutOrderInsert = (insertJSON, res) => {
    let FinalResult = '';
    connection.beginTransaction((err) => {
        if (err) {
            throw err;
        }
        const query = `INSERT INTO tblCheckout(Name, Telephone, Address, PayMode, userID) VALUES ('${insertJSON[0].UserName}','${insertJSON[0].PhoneNumber}','${insertJSON[0].Address}','cash','${insertJSON[0].UserID}')`;
        connection.query(query, (error, result) => {
            if (error) {
                return connection.rollback(() => {
                    throw error;
                });
            }
            if (result.insertId > 0) {
                for (i in insertJSON) {
                    // for loop
                    // const sizeObj = eval(`(${insertJSON.SizeArray})`);
                    const sizequery = `INSERT INTO tblOrder(productID, checkOutID, itemQuantity, orderDate) VALUES ('${insertJSON[i].ProductID}','${result.insertId}','${insertJSON[i].Quantity}',NOW())`;
                    connection.query(sizequery, (error1) => {
                        if (error1) {
                            return connection.rollback(() => {
                                throw error1;
                            });
                        }
                        // this is ProductAddInfo Update
                        const SelectProductAddInfo = `SELECT qty FROM tblProductAddInfo WHERE productID='${insertJSON[i].ProductID}' AND addInfoValue='${insertJSON[i].size}'`;
                        connection.query(SelectProductAddInfo, (error2, rows2) => {
                            if (error2) {
                                return connection.rollback(() => {
                                    throw error2;
                                });
                            }
                            if (rows2.length > 0) {
                                const itemQuty = rows2[0].qty - insertJSON[i].Quantity;
                                const UpdateQuery = `UPDATE tblProductAddInfo SET qty='${itemQuty}' WHERE productID = '${insertJSON[i].ProductID}' AND addInfoValue = '${insertJSON[i].size}'`;
                                connection.query(UpdateQuery, (error3) => {
                                    if (error3) {
                                        return connection.rollback(() => {
                                            throw error3;
                                        });
                                    }
                                    //this is Product Update
                                    const SelectProduct = `SELECT productQty FROM tblProduct WHERE product_id='${insertJSON[i].ProductID}'`;
                                    connection.query(SelectProduct, (error4, Productrows) => {
                                        if (error4) {
                                            return connection.rollback(() => {
                                                throw error4;
                                            });
                                        }
                                        if (Productrows.length > 0) {
                                            const ProductQuty = Productrows[0].productQty - insertJSON[i].Quantity;
                                            const UpdateProductQuery = `UPDATE tblProduct SET productQty='${ProductQuty}' WHERE product_id='${insertJSON[i].ProductID}'`;
                                            connection.query(UpdateProductQuery, (error5) => {
                                                if (error5) {
                                                    return connection.rollback(() => {
                                                        throw error5;
                                                    });
                                                }
                                            });
                                        }
                                    });
                                    // End Product Update                             
                                });
                            }
                        });
                        // End ProductAddInfo Update
                        connection.commit((queryerror) => {
                            if (queryerror) {
                                return connection.rollback(() => {
                                    throw queryerror;
                                });
                            }
                        });
                    });
                } // for loop
            }
            FinalResult = {
                status: 'Success',
                messege: 'Inserted Successfully'
            };
            res.json(FinalResult);
        });
        // CheckOut insert 
    });
};

const GetOrderForUser = (req, res) => {
    let FinalResult = '';
    let productIds = '';
    const query = `SELECT orderID, productID, checkOutID, itemQuantity, orderDate FROM tblOrder WHERE checkOutID in (SELECT CheckOutID userID FROM tblcheckout WHERE userID=${req.params.UserID})`;
    connection.query(query, (error, rows) => {
        if (error) {
            FinalResult = { status: 'Failure', Msg: 'Error in fetching orders' };
            res.json(FinalResult);
        } else {
            if (rows.length > 0) {
                for (let i = 0; i < rows.length; i++) {
                    productIds += `${rows[i].productID},`;
                }
                productIds = productIds.slice(0, -1);
                const queryProd = `SELECT tblP.product_id AS ProductID,tblP.device_id AS DeviceID,tblP.category_id AS CategoryID,tblP.sub_category_id AS SubCategoryID,tblP.product_name AS ProductName,tblP.product_desc AS ProductDesc,tblP.productImg AS ProductImage,tblP.productImg1 AS ProductImage1,tblP.productImg2 AS ProductImage2,tblP.productImg3 AS ProductImage3,tblP.productImg4 AS ProductImage4,tblP.productImg5 AS ProductImage5,tblP.isActive AS IsActive,tblP.productCost AS ProductCost,tblP.productCurrency AS ProductCurrency,tblP.productQty AS ProductQty,tblP.productSize AS ProductSize,tblP.productSex AS ProductSex,tblP.percentDiscount AS PercentDiscount,tblP.finalAmount AS finalAmount,tblC.category_name as CategoryName, tblSC.subCategory_name as SubCategoryName,tblP.shop_ID AS ShopID from tblProduct tblP INNER JOIN tblCategory tblC ON tblP.category_id=tblC.category_id INNER JOIN tblSubCategory tblSC ON tblP.sub_category_id=tblSC.subCategoryID  WHERE tblP.isActive=1 AND tblP.product_id in (${productIds})`;
                connection.query(queryProd, (err, rowsProd) => {
                    FinalResult = { status: 'success', record_count: rows.length, records: rowsProd };
                    res.json(FinalResult);
                });
            } else {
                FinalResult = { status: 'Failure', Msg: 'Error in fetching orders' };
                res.json(FinalResult);
            }
        }
    });
};


module.exports.SelectAllOrders = SelectAllOrders;
module.exports.insertOrder = insertOrder;
module.exports.CheckoutOrderInsert = CheckoutOrderInsert;
module.exports.GetOrderForUser = GetOrderForUser;

