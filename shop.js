const connection = require('./const').connection;

let FinalResult = '';

const SelectAllShop = (req, res) => {
    connection.query('SELECT shopID,shopName,shopDesc,shopTinNumber,shopAddress,shopEmail,shopPhone,shopLogo,shopUserName,shopPassWord,isShop from tblShop', (error, rows) => {
        if (!error) {
                FinalResult = { status: 'success', record_count: rows.length, records: rows };
                res.json(FinalResult);
            } else {
                FinalResult = { status: 'Failure', Msg: 'Error in fetching shop' };
                res.json(FinalResult);
            }
});
};

const insertShop = (insertJSON, res) => {
    const query = `INSERT INTO tblShop(shopName,shopDesc,shopTinNumber,shopAddress,shopEmail,shopPhone,shopLogo, shopUserName,shopPassWord,isShop) values('${insertJSON.shopName}','${insertJSON.shopDesc}','${insertJSON.shopTinNumber}','${insertJSON.shopAddress}','${insertJSON.shopEmail}','${insertJSON.shopPhone}','${insertJSON.shopLogo}',
    '${insertJSON.shopUserName}','${insertJSON.shopPassWord}','${insertJSON.isShop}')`;
    connection.query(query, (error) => {
        if (error) {
            FinalResult = { status: 'failure', messege: 'Insertion Failed' };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'success', messege: 'Inserted Successfully' };
            res.json(FinalResult);
        }
    });
};

const updateShop = (updateJSON, res) => {
    const query = `UPDATE tblShop SET shopName='${updateJSON.shopName}',shopDesc='${updateJSON.shopDesc}',shopTinNumber='${updateJSON.shopTinNumber}',shopAddress='${updateJSON.shopAddress}',shopEmail='${updateJSON.shopEmail}',shopPhone='${updateJSON.shopPhone}',shopLogo='${updateJSON.shopLogo}',shopUserName='${updateJSON.shopUserName}',shopPassWord='${updateJSON.shopPassWord}',isShop='${updateJSON.isShop}' WHERE shopID='${updateJSON.shopID}'`;
    connection.query(query, (error) => {
        if (error) {
            FinalResult = { status: 'failure', messege: 'Insertion Failed' };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'success', messege: 'Updated Successfully' };
            res.json(FinalResult);
        }
    });
};

const SelectShop = (req, res) => {
    const query = `SELECT shopID,shopName,shopDesc,shopTinNumber,shopAddress,shopEmail,shopPhone,shopLogo,shopUserName,shopPassWord,isShop FROM tblShop WHERE shopID=${req.params.id}`;
    connection.query(query, (error, rows) => {
        if (!error) {
            FinalResult = { status: 'success', record_count: rows.length, records: rows };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'Failure', Msg: 'Error in fetching shop' };
            res.json(FinalResult);
        }
    });
};  

module.exports.SelectAllShop = SelectAllShop;
module.exports.insertShop = insertShop;
module.exports.updateShop = updateShop;
module.exports.SelectShop = SelectShop;
