const connection = require('./const').connection;

let FinalResult = '';


const SelectAllProduct = (req, res) => {
    connection.query('SELECT tblP.product_id AS ProductID,tblP.device_id AS DeviceID,tblP.category_id AS CategoryID,tblP.sub_category_id AS SubCategoryID,tblP.brand_id AS BrandID,tblP.product_name AS ProductName,tblP.product_desc AS ProductDesc,tblP.productImg AS ProductImage,tblP.productImg1 AS ProductImage1,tblP.productImg2 AS ProductImage2,tblP.productImg3 AS ProductImage3,tblP.productImg4 AS ProductImage4,tblP.productImg5 AS ProductImage5,tblP.isActive AS IsActive,tblP.productCost AS ProductCost,tblP.productCurrency AS ProductCurrency,tblP.productQty AS ProductQty,tblP.productSize AS ProductSize,tblP.productSex AS ProductSex,tblP.percentDiscount AS PercentDiscount,tblP.finalAmount AS finalAmount,tblC.category_name as CategoryName, tblSC.subCategory_name as SubCategoryName,tblP.shop_ID AS ShopID from tblProduct tblP INNER JOIN tblCategory tblC ON tblP.category_id=tblC.category_id INNER JOIN tblSubCategory tblSC ON tblP.sub_category_id=tblSC.subCategoryID  WHERE tblP.isActive=1 ORDER BY tblP.product_id DESC', (error, rows) => {
        if (!error) {
            FinalResult = { status: 'success', record_count: rows.length, records: rows };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'Failure', Msg: 'Error in fetching product' };
            res.json(FinalResult);
        }
});
};

const insertProduct = (insertJSON, res) => {
   connection.beginTransaction((err) => {
     if (err) { throw err; }
     const query = `INSERT INTO tblProduct(device_id,category_id,sub_category_id,brand_id,product_name,product_desc, productImg, productImg1, productImg2, productImg3,productCost,percentDiscount,productQty,finalAmount) values('${insertJSON.devicId}','${insertJSON.CategoryID}','${insertJSON.SubCategoryID}','${insertJSON.BrandID}','${insertJSON.ProductName}','${insertJSON.ProductDesc}','${insertJSON.ProductImage}','${insertJSON.ProductImage1}','${insertJSON.ProductImage2}','${insertJSON.ProductImage3}','${insertJSON.ProductCost}','${insertJSON.ProductDiscount}','${insertJSON.ProductQty}','${insertJSON.FinalAmount}')`;
     connection.query(query, (error, result) => {
        if (error) {
        return connection.rollback(() => {
            throw error;
        });
        } 
           // const sizeObj = eval(`(${insertJSON.SizeArray})`);
            if (insertJSON.SizeArray.length > 0) {
            for (i in insertJSON.SizeArray) {
            const sizequery = `INSERT INTO tblProductAddInfo(addInfoType,addInfoValue,productID,qty) VALUES ('Size','${insertJSON.SizeArray[i].Size}','${result.insertId}','${insertJSON.SizeArray[i].sizeProductQty}')`;
            connection.query(sizequery, (error1) => {
                if (error1) {
            return connection.rollback(() => {
              throw error1;
            });
          }  
          connection.commit((queryerror) => {
            if (queryerror) {
              return connection.rollback(() => {
                throw queryerror;
              });
            }
          });              
        });
        }
        }
         FinalResult = { status: 'Success', messege: 'Inserted Successfully' };
         res.json(FinalResult);
    });
   });    
};


const updateProduct = (updateJSON, res) => {
    const query = `UPDATE tblProduct SET category_id='${updateJSON.CategoryID}',sub_category_id='${updateJSON.SubCategoryID}',brand_id='${updateJSON.BrandID}',product_name='${updateJSON.ProductName}',product_desc='${updateJSON.ProductDesc}',productImg='${updateJSON.ProductImage}',productImg1='${updateJSON.ProductImage1}',productImg2='${updateJSON.ProductImage2}',productImg3='${updateJSON.ProductImage3}',productCost='${updateJSON.ProductCost}',percentDiscount='${updateJSON.ProductDiscount}',productQty='${updateJSON.ProductQty}',finalAmount='${updateJSON.FinalAmount}' WHERE product_id='${updateJSON.ProductID}'`;
    connection.query(query, (error) => {
        if (error) {
            return connection.rollback(() => {
                throw error;
            });
        } else {
            const DeleteQuery = `DELETE FROM tblProductAddInfo WHERE productID ='${updateJSON.ProductID}'`;
            connection.query(DeleteQuery, (error1) => {
                if (error1) {
                    return connection.rollback(() => {
                        throw error1;
                    });
                } else {
                    if (updateJSON.SizeArray.length > 0) {
                        for (i in updateJSON.SizeArray) {
                            const sizequery = `INSERT INTO tblProductAddInfo(addInfoType,addInfoValue,productID,qty) VALUES ('Size','${updateJSON.SizeArray[i].Size}','${updateJSON.ProductID}','${updateJSON.SizeArray[i].sizeProductQty}')`;
                            connection.query(sizequery, (error2) => {
                                if (error2) {
                                    return connection.rollback(() => {
                                        throw error2;
                                    });
                                }
                                connection.commit((queryerror) => {
                                    if (queryerror) {
                                        return connection.rollback(() => {
                                            throw queryerror;
                                        });
                                    }
                                });
                            });
                        }
                    }
                    FinalResult = {
                        status: 'Success',
                        messege: 'Updated Successfully'
                    };
                    res.json(FinalResult);
                }
            });
        }
    });
};

const DeleteProduct = (req, res) => {
    const query = `UPDATE tblProduct SET isActive=0 WHERE product_ID='${req.params.id}'`;
    connection.query(query, (error) => {
        if (error) {
            FinalResult = { status: 'failure', messege: 'Failed to delete' };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'Success', messege: 'Product Deleted Successfully' };
            res.json(FinalResult);
        }
    });
};

const SelectProduct = (req, res) => {
    const ResultArray = [];
    const query = `SELECT tblP.product_id AS ProductID,tblP.device_id AS DeviceID,tblP.category_id AS CategoryID,tblP.sub_category_id AS SubCategoryID,tblP.product_name AS ProductName,tblP.product_desc AS ProductDesc,tblP.productImg AS ProductImage,tblP.productImg1 AS ProductImage1,tblP.productImg2 AS ProductImage2,tblP.productImg3 AS ProductImage3,tblP.productImg4 AS ProductImage4,tblP.productImg5 AS ProductImage5,tblP.isActive AS IsActive,tblP.productCost AS ProductCost,tblP.productCurrency AS ProductCurrency,tblP.productQty AS ProductQty,tblP.productSize AS ProductSize,tblP.productSex AS ProductSex,tblP.percentDiscount AS PercentDiscount,tblP.finalAmount AS finalAmount,tblC.category_name as CategoryName, tblSC.subCategory_name as SubCategoryName,tblP.shop_ID AS ShopID from tblProduct tblP INNER JOIN tblCategory tblC ON tblP.category_id=tblC.category_id INNER JOIN tblSubCategory tblSC ON tblP.sub_category_id=tblSC.subCategoryID  WHERE tblP.isActive=1 AND tblP.product_id=${req.params.id}`;
    connection.query(query, (error, rows) => {
        if (error) { throw error; } else {
            const ProductID = rows[0].ProductID;
            console.log(ProductID);
            if (ProductID !== undefined) {
                 const productInfoQuery = `SELECT addInfoID,addInfoType,addInfoValue AS size,productID,qty AS sizeQty FROM tblProductAddInfo WHERE productID=${ProductID}`;
                 connection.query(productInfoQuery, (error2, rows2) => {
                    if (!error2) {
                        for (let i = 0; i < rows.length; i++) {
                            ResultArray.push(rows[i]);
                            ResultArray[i].ProductInfo = rows2;
                        }
                    FinalResult = { status: 'success', record_count: rows.length, records: ResultArray };
                    res.json(FinalResult);
                } else {
                    FinalResult = { status: 'Failure', Msg: 'Error in fetching product' };
                    res.json(FinalResult);
                }        
            });
            } else {
                    FinalResult = { status: 'Failure', Msg: 'Error in fetching product' };
                    res.json(FinalResult);
                }  
        }      
    });
}; 


const GetProductBySequence = (PostJSON, res) => {
    const query = `SELECT product_id,device_id,category_id,sub_category_id,brand_id,product_name,product_desc,productImg,productImg,productImg1,productImg2,productImg3,productImg4,productImg5,shop_ID,isActive,productCost,productCurrency,productQty,productSize,productSex,percentDiscount,finalAmount FROM tblProduct WHERE category_id=
                   ${PostJSON.categoryID} AND sub_category_id=${PostJSON.subCategoryID} AND brand_id=${PostJSON.brandID}`; 
    connection.query(query, (error, rows) => {
        if (!error) {
            FinalResult = { status: 'success', record_count: rows.length, records: rows };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'Failure', Msg: 'Error While Fetch in records' };
            res.json(FinalResult);
        }
    });
};

const GetProductBasedOnCategory = (req, res) => {
    const query = `SELECT product_id,device_id,category_id,sub_category_id,product_name,product_desc,productImg,productImg1,productImg2,productImg3,productImg4,productImg5,shop_ID,isActive,productCost,productCurrency,productQty,productSize,productSex,percentDiscount,finalAmount FROM tblProduct WHERE category_id=${req.params.categotryId} AND sub_category_id=${req.params.subCategotryId}`;
    connection.query(query, (error, rows) => {
        if (!error) {
            FinalResult = { status: 'success', record_count: rows.length, records: rows };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'Failure', Msg: 'Error in Fetching Product' };
            res.json(FinalResult);
        }
    });
}; 

const GetRandomProduct = (req, res) => {
    const query = 'SELECT product_id,device_id,category_id,product_name,product_desc,productImg,productImg1,productImg2,productImg3,productImg4,productImg5,shop_ID,isActive,productCost,productCurrency,productQty,productSize,productSex,percentDiscount,finalAmount FROM tblProduct ORDER BY RAND() LIMIT 12';
    connection.query(query, (error, rows) => {
        if (!error) {
            FinalResult = { status: 'success', record_count: rows.length, records: rows };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'Failure', Msg: 'Error in Fetching Product' };
            res.json(FinalResult);
        }
    });
}; 

const GetProductInfoByProductID = (req, res) => {
    const query = `SELECT addInfoID,addInfoType,addInfoValue,productID,qty FROM tblProductAddInfo WHERE productID=${req.params.productId}`;
    connection.query(query, (error, rows) => {
        if (!error) {
            FinalResult = { status: 'success', record_count: rows.length, records: rows };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'Failure', Msg: 'Error in Fetching Product' };
            res.json(FinalResult);
        }
    });
}; 

module.exports.SelectAllProduct = SelectAllProduct;
module.exports.insertProduct = insertProduct;
module.exports.updateProduct = updateProduct;
module.exports.DeleteProduct = DeleteProduct;
module.exports.SelectProduct = SelectProduct;
module.exports.GetProductBySequence = GetProductBySequence;
module.exports.GetProductBasedOnCategory = GetProductBasedOnCategory;
module.exports.GetRandomProduct = GetRandomProduct;
module.exports.GetProductInfoByProductID = GetProductInfoByProductID;
