const connection = require('./const').connection;

   let FinalResult = '';

const SelectAllSubCategory = (req, res) => {
 connection.query('SELECT tblSub.category_id,tblSub.subCategoryID AS SubCategoryID,tblSub.subCategory_name AS SubCategoryName,tblSub.shop_ID,tblC.category_name from tblSubcategory tblSub INNER JOIN tblCategory tblC on tblC.category_id=tblSub.category_id ORDER BY tblSub.subCategoryID DESC', (error, rows) => {
            if (!error) {
                FinalResult = { status: 'success', record_count: rows.length, records: rows };
                res.json(FinalResult);
            } else {
                FinalResult = { status: 'Failure', Msg: 'Error in fetching Subcategory' };
                res.json(FinalResult);
            }
    });
}; 

const InsertSubCategory = (insertJSON, res) => {
    const query = `INSERT INTO tblSubCategory(category_id,subCategory_name) VALUES ('${insertJSON.categoryID}','${insertJSON.subCategoryName}')`;
    connection.query(query, (error) => {
        if (error) {
            FinalResult = { status: 'failure', messege: 'Insertion Failed' };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'success', messege: 'Inserted Successfully' };
            res.json(FinalResult);
        }
    });
};
const UpdateSubCategory = (updateJSON, res) => {
    const query = `UPDATE tblSubCategory SET subCategory_name='${updateJSON.subCategoryName}',category_id='${updateJSON.categoryID}' WHERE subCategoryID = '${updateJSON.subCategoryID}'`;
    connection.query(query, (error) => {
        if (error) {
            FinalResult = { status: 'failure', messege: 'Update Failed' };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'success', messege: 'Updated Successfully' };
            res.json(FinalResult);
        }
    });
};

const SelectSubCategory = (req, res) => {
    const subCategoryID = req.params.id;
    connection.query('SELECT tblSub.category_id,tblSub.subCategoryID,tblSub.subCategory_name,tblSub.shop_ID,tblC.category_name from tblSubcategory tblSub INNER JOIN tblCategory tblC on tblC.category_id=tblSub.category_id WHERE tblSub.subCategoryID= ?', [subCategoryID], (error, rows) => {
        if (!error) {
            FinalResult = { status: 'success', record_count: rows.length, records: rows };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'Failure', Msg: 'Error in fetching Subcategory' };
            res.json(FinalResult);
        }
    });
};

const AllSubCategoryByCategotry = (req, res) => {
    const CategoryID = req.params.categotryId;
    connection.query('SELECT category_id AS CategoryID,subCategoryID AS SubCategoryID,subCategory_name AS SubCategoryName, shop_ID FROM tblSubCategory WHERE category_id= ?', [CategoryID], (error, rows) => {
        if (!error) {
            FinalResult = { status: 'success', record_count: rows.length, records: rows };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'Failure', Msg: 'Error in fetching Subcategory' };
            res.json(FinalResult);
        }
    });
};

const DeleteSubCategory = (PostJSON, res) => {
    //const subCategoryID = req.params.id;
        const query = `DELETE FROM tblSubCategory WHERE subCategoryID='${PostJSON.SubCategoryID}'`;
        connection.query(query, (error) => {
        if (error) {
            FinalResult = { status: 'failure', messege: 'Insertion Failed' };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'success', messege: 'Delete Successfully' };
            res.json(FinalResult);
        }
    });
};

module.exports.SelectAllSubCategory = SelectAllSubCategory;
module.exports.InsertSubCategory = InsertSubCategory;
module.exports.UpdateSubCategory = UpdateSubCategory;
module.exports.SelectSubCategory = SelectSubCategory;
module.exports.AllSubCategoryByCategotry = AllSubCategoryByCategotry;
module.exports.DeleteSubCategory = DeleteSubCategory;
